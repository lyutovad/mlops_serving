FROM python:3.10.12-slim-buster 
WORKDIR /
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["python", "src/server.py"]
